package io.piano.stackexchange

import io.piano.stackexchange.dto.question.Question
import io.piano.stackexchange.dto.question.QuestionsSearchResponse
import io.piano.stackexchange.service.StackOverflowService
import io.piano.stackexchange.service.impl.StackOverflowServiceImpl
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.web.client.RestTemplate
import spock.lang.Specification
import spock.mock.DetachedMockFactory

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class StackOverflowControllerIS extends Specification {

    @Autowired
    MockMvc mvc

    @Autowired
    StackOverflowService stackOverflowService

    def "check StackOverflow controller available"() {
        def question = new Question()
        question.questionId = 1
        def results = new ArrayList<Question>()
        results << question
        given:
        stackOverflowService.findQuestionsByTitle(_) >> results

        expect: "controller is available and questions return"
        mvc.perform(MockMvcRequestBuilders.get("/api/stackoverflow/questions"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("[0].questionId").value("1"))
    }

}
