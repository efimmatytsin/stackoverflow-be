package io.piano.stackexchange.dto.question;

import lombok.Data;

import java.util.List;

@Data
public class QuestionsSearchResponse {
    private List<Question> items;
}
