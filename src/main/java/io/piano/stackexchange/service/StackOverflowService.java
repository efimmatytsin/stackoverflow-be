package io.piano.stackexchange.service;

import io.piano.stackexchange.dto.question.Question;

import java.util.List;

public interface StackOverflowService {
    List<Question> findQuestionsByTitle(String title);
}
